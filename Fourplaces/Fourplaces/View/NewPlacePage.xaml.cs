﻿using Fourplaces.ViewModel;
using Storm.Mvvm.Forms;

namespace Fourplaces.View
{
    public partial class NewPlacePage : BaseContentPage
    {
        public NewPlacePage()
        {
            BindingContext = new NewPlaceViewModel();
            InitializeComponent();
        }
    }
}
