﻿using Fourplaces.ViewModel;
using Storm.Mvvm.Forms;

namespace Fourplaces.View
{
    public partial class ProfilePage : BaseContentPage
    {
        public ProfilePage()
        {
            BindingContext = new ProfileViewModel();
            InitializeComponent();
        }
    }
}