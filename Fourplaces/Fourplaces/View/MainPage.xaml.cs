﻿using Fourplaces.ViewModel;
using Storm.Mvvm.Forms;

namespace Fourplaces.View
{
    public partial class MainPage : BaseContentPage
    {
        public MainPage()
        {
            BindingContext = new MainPageViewModel();
            InitializeComponent();
        }
    }
}