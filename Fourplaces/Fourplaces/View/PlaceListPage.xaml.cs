﻿using Fourplaces.ViewModel;
using Storm.Mvvm.Forms;

namespace Fourplaces.View
{
    public partial class PlaceListPage : BaseContentPage
    {
        public PlaceListPage()
        {
            BindingContext = new PlaceListViewModel();
            InitializeComponent();
        }
    }
}
