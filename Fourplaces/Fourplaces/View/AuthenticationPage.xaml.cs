﻿using Fourplaces.ViewModel;
using Storm.Mvvm.Forms;

namespace Fourplaces.View
{
    public partial class AuthenticationPage : BaseContentPage
    {
        public AuthenticationPage()
        {
            BindingContext = new AuthenticationViewModel();
            InitializeComponent();
        }
    }
}
