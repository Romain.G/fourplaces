﻿using Fourplaces.ViewModel;
using Storm.Mvvm.Forms;

namespace Fourplaces.View
{
    public partial class PlaceItemPage : BaseContentPage
    {
        public PlaceItemPage()
        {
            BindingContext = new PlaceItemViewModel();
            InitializeComponent();
        }
    }
}
