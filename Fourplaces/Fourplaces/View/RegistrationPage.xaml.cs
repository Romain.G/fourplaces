﻿using Fourplaces.ViewModel;
using Storm.Mvvm.Forms;

namespace Fourplaces.View
{
    public partial class RegistrationPage : BaseContentPage
    {
        public RegistrationPage()
        {
            BindingContext = new RegistrationViewModel();
            InitializeComponent();
        }
    }
}
