﻿using Fourplaces.ViewModel;
using Storm.Mvvm.Forms;

namespace Fourplaces.View
{
    public partial class CommentListPage : BaseContentPage
    {
        public CommentListPage()
        {
            BindingContext = new CommentListViewModel();
            InitializeComponent();
        }
    }
}