using Newtonsoft.Json;

namespace Fourplaces.DTO
{
	public class CreateCommentRequest
	{
		[JsonProperty("text")]
		public string Text { get; set; }
	}
}