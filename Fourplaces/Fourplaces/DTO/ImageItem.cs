using Newtonsoft.Json;

namespace Fourplaces.DTO
{
	public class ImageItem
	{
		[JsonProperty("id")]
		public int Id { get; set; }
	}
}