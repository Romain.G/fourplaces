using Newtonsoft.Json;

namespace Fourplaces.DTO
{
    public class RefreshRequest
    {
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
    }
}