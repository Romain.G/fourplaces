﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Fourplaces.DTO;
using Newtonsoft.Json;

namespace Fourplaces.API
{
	public class ApiClient
    {
		public const string API_BASE = "https://td-api.julienmialon.com";
		public const string API_AUTH = "/auth";
		public const string API_LOGIN = "/login";
		public const string API_REGISTER = "/register";
		public const string API_PLACES = "/places";
		public const string API_IMAGES = "/images";
		public const string API_COMMENTS = "/comments";
		public const string API_ME = "/me";

		private readonly HttpClient _client = new HttpClient();

		public async Task<HttpResponseMessage> Execute(HttpMethod method, string url, object data = null, string token = null)
		{
			HttpRequestMessage request = new HttpRequestMessage(method, url);

			if (data != null)
			{
				request.Content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
			}

			if (token != null)
			{
				request.Headers.Add("Authorization", $"Bearer {token}");
			}

			return await _client.SendAsync(request);
		}

		public async Task<T> ReadFromResponse<T>(HttpResponseMessage response)
		{
			string result = await response.Content.ReadAsStringAsync();

			return JsonConvert.DeserializeObject<T>(result);
		}

		public async Task<Response<ImageItem>> SendImageAsync(byte[] bytes, string token)
		{
			HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, API_BASE + API_IMAGES);
			request.Headers.Add("Authorization", $"Bearer {token}");

			MultipartFormDataContent requestContent = new MultipartFormDataContent();
			ByteArrayContent imageContent = new ByteArrayContent(bytes);
			imageContent.Headers.ContentType = MediaTypeHeaderValue.Parse("image/jpeg");

			// Le deuxième paramètre doit absolument être "file" ici sinon ça ne fonctionnera pas
			requestContent.Add(imageContent, "file", "file.jpg");

			request.Content = requestContent;

			HttpResponseMessage response = await _client.SendAsync(request);

			return await ReadFromResponse<Response<ImageItem>>(response);
		}
	}
}
