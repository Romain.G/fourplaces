﻿using System;
using System.Net.Http;
using System.Windows.Input;
using Fourplaces.API;
using Fourplaces.DTO;
using Fourplaces.View;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Fourplaces.ViewModel
{
    class RegistrationViewModel : ViewModelBase
    {
        private readonly Lazy<ICurrentPageService> _currentPageService;
        private readonly Lazy<INavigationService> _navigationService;
        private readonly Lazy<IDialogService> _dialogService;
        private readonly ApiClient _apiClient;

        private string _email;
        private string _firstName;
        private string _lastName;
        private string _password;

        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        public string FirstName
        {
            get => _firstName;
            set => SetProperty(ref _firstName, value);
        }

        public string LastName
        {
            get => _lastName;
            set => SetProperty(ref _lastName, value);
        }

        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        public ICommand RegisterCommand
        {
            get;
        }

        public RegistrationViewModel()
        {
            _currentPageService = new Lazy<ICurrentPageService>(() => DependencyService.Resolve<ICurrentPageService>());
            _navigationService = new Lazy<INavigationService>(() => DependencyService.Resolve<INavigationService>());
            _dialogService = new Lazy<IDialogService>(() => DependencyService.Resolve<IDialogService>());
            _apiClient = new ApiClient();
            RegisterCommand = new Command(RegisterActionAsync);
        }

        public async void RegisterActionAsync()
        {
            if (string.IsNullOrEmpty(Email) || string.IsNullOrEmpty(FirstName) || string.IsNullOrEmpty(LastName) || string.IsNullOrEmpty(Password))
            {
                await _dialogService.Value.DisplayAlertAsync("Missing fields", "", "OK");
                return;
            }
            RegisterRequest registerRequest = new RegisterRequest
            {
                Email = Email,
                FirstName = FirstName,
                LastName = LastName,
                Password = Password
            };
            HttpResponseMessage httpResponse;
            try
            {
                httpResponse = await _apiClient.Execute(HttpMethod.Post, ApiClient.API_BASE + ApiClient.API_AUTH + ApiClient.API_REGISTER, registerRequest);
            }
            catch
            {
                await _dialogService.Value.DisplayAlertAsync("Unable to connect", "", "OK");
                return;
            }

            Response<LoginResult> response = await _apiClient.ReadFromResponse<Response<LoginResult>>(httpResponse);
            if (response.IsSuccess)
            {
                await SecureStorage.SetAsync("token", response.Data.AccessToken);
                await _dialogService.Value.DisplayAlertAsync("Registered successfully!", "", "OK");
                _currentPageService.Value.CurrentPage.Navigation.InsertPageBefore(new MainPage(), _currentPageService.Value.CurrentPage);
                await _navigationService.Value.PopAsync();
            }
            else
            {
                await _dialogService.Value.DisplayAlertAsync("Unable to register", "", "OK");
            }
        }
    }
}
