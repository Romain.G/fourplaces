﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Input;
using Fourplaces.API;
using Fourplaces.DTO;
using Storm.Mvvm;
using Storm.Mvvm.Navigation;
using Storm.Mvvm.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Fourplaces.ViewModel
{
    public class CommentListViewModel : ViewModelBase
    {
        private readonly Lazy<IDialogService> _dialogService;
        private readonly ApiClient _apiClient;

        private string _newComment;

        private PlaceItemSummary _placeSummary;

        public ObservableCollection<CommentItem> CommentList
        {
            get;
        }

        public string NewComment
        {
            get => _newComment;
            set => SetProperty(ref _newComment, value);
        }

        public ICommand SendCommentCommand
        {
            get;
        }

        [NavigationParameter("PlaceItemSummary")]
        private PlaceItemSummary PlaceItemSummary
        {
            get;
            set;
        }

        public PlaceItemSummary PlaceSummary
        {
            get => _placeSummary;
            set => SetProperty(ref _placeSummary, value);
        }

        public CommentListViewModel()
        {
            _dialogService = new Lazy<IDialogService>(() => DependencyService.Resolve<IDialogService>());
            _apiClient = new ApiClient();

            CommentList = new ObservableCollection<CommentItem>();
            SendCommentCommand = new Command(SendCommentActionAsync);
        }

        public override void Initialize(Dictionary<string, object> navigationParameters)
        {
            base.Initialize(navigationParameters);
            PlaceSummary = PlaceItemSummary;
        }

        public override async Task OnResume()
        {
            await base.OnResume();

            PopulateCommentListAsync();
        }

        public async void PopulateCommentListAsync()
        {
            HttpResponseMessage httpResponse;
            try
            {
                httpResponse = await _apiClient.Execute(HttpMethod.Get, ApiClient.API_BASE + ApiClient.API_PLACES + "/" + PlaceSummary.Id);
            }
            catch
            {
                await _dialogService.Value.DisplayAlertAsync("Unable to connect", "", "OK");
                return;
            }

            Response<PlaceItem> response = await _apiClient.ReadFromResponse<Response<PlaceItem>>(httpResponse);
            if (response.IsSuccess)
            {
                CommentList.Clear();
                foreach (CommentItem comment in response.Data.Comments)
                {
                    CommentList.Add(comment);
                }
            }
            else
            {
                await _dialogService.Value.DisplayAlertAsync("Unable to retrieve comments", "", "OK");
            }
        }

        public async void SendCommentActionAsync()
        {
            if (string.IsNullOrEmpty(NewComment))
            {
                await _dialogService.Value.DisplayAlertAsync("Field is empty", "", "OK");
                return;
            }
            CreateCommentRequest commentRequest = new CreateCommentRequest
            {
                Text = NewComment
            };
            string token = await SecureStorage.GetAsync("token");

            HttpResponseMessage httpResponse;
            try
            {
                httpResponse = await _apiClient.Execute(HttpMethod.Post, ApiClient.API_BASE + ApiClient.API_PLACES + "/" + PlaceSummary.Id + ApiClient.API_COMMENTS, commentRequest, token);
            }
            catch
            {
                await _dialogService.Value.DisplayAlertAsync("Unable to connect", "", "OK");
                return;
            }

            Response response = await _apiClient.ReadFromResponse<Response>(httpResponse);
            if (response.IsSuccess)
            {
                NewComment = "";
                PopulateCommentListAsync();
            }
            else
            {
                await _dialogService.Value.DisplayAlertAsync("Unable to send comment", "", "OK");
            }
        }
    }
}
