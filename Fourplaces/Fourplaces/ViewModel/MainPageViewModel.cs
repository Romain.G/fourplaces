﻿using System;
using System.Windows.Input;
using Fourplaces.View;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using Xamarin.Forms;

namespace Fourplaces.ViewModel
{
    public class MainPageViewModel : ViewModelBase
    {
        private readonly Lazy<INavigationService> _navigationService;

        public ICommand PlacesCommand
        {
            get;
        }

        public ICommand ProfileCommand
        {
            get;
        }

        public MainPageViewModel()
        {
            _navigationService = new Lazy<INavigationService>(() => DependencyService.Resolve<INavigationService>());

            PlacesCommand = new Command(PlacesActionAsync);
            ProfileCommand = new Command(ProfileActionAsync);
        }

        public async void PlacesActionAsync()
        {
            await _navigationService.Value.PushAsync<PlaceListPage>();
        }

        public async void ProfileActionAsync()
        {
            await _navigationService.Value.PushAsync<ProfilePage>();
        }
    }
}
