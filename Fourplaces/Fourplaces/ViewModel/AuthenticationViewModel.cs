﻿using System;
using System.Net.Http;
using System.Windows.Input;
using Fourplaces.API;
using Fourplaces.DTO;
using Fourplaces.View;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Fourplaces.ViewModel
{
    public class AuthenticationViewModel : ViewModelBase
    {
        private readonly Lazy<INavigationService> _navigationService;
        private readonly Lazy<IDialogService> _dialogService;
        private readonly ApiClient _apiClient;

        private string _email;
        private string _password;

        public AuthenticationViewModel()
        {
            _navigationService = new Lazy<INavigationService>(() => DependencyService.Resolve<INavigationService>());
            _dialogService = new Lazy<IDialogService>(() => DependencyService.Resolve<IDialogService>());
            _apiClient = new ApiClient();
            LogInCommand = new Command(LogInActionAsync);
            RegisterCommand = new Command(RegisterActionAsync);
        }

        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        public ICommand LogInCommand
        {
            get;
        }

        public ICommand RegisterCommand
        {
            get;
        }

        public async void LogInActionAsync()
        {
            LoginRequest loginRequest = new LoginRequest
            {
                Email = Email,
                Password = Password,
            };

            HttpResponseMessage httpResponse;
            try
            {
                httpResponse = await _apiClient.Execute(HttpMethod.Post, ApiClient.API_BASE + ApiClient.API_AUTH + ApiClient.API_LOGIN, loginRequest);
            }
            catch
            {
                await _dialogService.Value.DisplayAlertAsync("Unable to connect", "", "OK");
                return;
            }
            
            Response<LoginResult> response = await _apiClient.ReadFromResponse<Response<LoginResult>>(httpResponse);
            if (response.IsSuccess)
            {
                await SecureStorage.SetAsync("token", response.Data.AccessToken);
                await _navigationService.Value.PushAsync<MainPage>();
            }
            else
            {
                await _dialogService.Value.DisplayAlertAsync("Invalid email or password", "", "OK");
            }
        }

        public async void RegisterActionAsync()
        {
            await _navigationService.Value.PushAsync<RegistrationPage>();
        }
    }
}
