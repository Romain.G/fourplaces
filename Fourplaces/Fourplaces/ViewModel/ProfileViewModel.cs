﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Fourplaces.API;
using Fourplaces.DTO;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Fourplaces.ViewModel
{
    public class ProfileViewModel : ViewModelBase
    {
        private readonly Lazy<IDialogService> _dialogService;
        private readonly ApiClient _apiClient;

        private UserItem _me;

        public UserItem Me
        {
            get => _me;
            set => SetProperty(ref _me, value);
        }

        public ProfileViewModel()
        {
            _dialogService = new Lazy<IDialogService>(() => DependencyService.Resolve<IDialogService>());
            _apiClient = new ApiClient();
        }

        public async override Task OnResume()
        {
            await base.OnResume();

            string token = await SecureStorage.GetAsync("token");
            HttpResponseMessage httpResponse;
            try
            {
                httpResponse = await _apiClient.Execute(HttpMethod.Get, ApiClient.API_BASE + ApiClient.API_ME, token: token);
            }
            catch
            {
                await _dialogService.Value.DisplayAlertAsync("Unable to connect", "", "OK");
                return;
            }

            Response<UserItem> response = await _apiClient.ReadFromResponse<Response<UserItem>>(httpResponse);
            if (response.IsSuccess)
            {
                Me = response.Data;
            }
            else
            {
                await _dialogService.Value.DisplayAlertAsync("Unable to retrieve profile", "", "OK");
            }
        }
    }
}
