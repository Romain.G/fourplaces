﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Input;
using Fourplaces.API;
using Fourplaces.DTO;
using Fourplaces.View;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using Xamarin.Forms;

namespace Fourplaces.ViewModel
{
    class PlaceListViewModel : ViewModelBase
    {
        private readonly Lazy<IDialogService> _dialogService;
        private readonly Lazy<INavigationService> _navigationService;
        private readonly ApiClient _apiClient;

        public ObservableCollection<PlaceItemSummary> PlaceList
        {
            get;
        }

        public ICommand ClickPlaceCommand
        {
            get;
        }

        public ICommand ClickNewPlaceCommand
        {
            get;
        }

        public PlaceListViewModel()
        {
            _dialogService = new Lazy<IDialogService>(() => DependencyService.Resolve<IDialogService>());
            _navigationService = new Lazy<INavigationService>(() => DependencyService.Resolve<INavigationService>());
            _apiClient = new ApiClient();

            PlaceList = new ObservableCollection<PlaceItemSummary>();
            ClickPlaceCommand = new Command<PlaceItemSummary>(ClickPlaceActionAsync);
            ClickNewPlaceCommand = new Command(ClickNewPlaceActionAsync);
        }

        public override async Task OnResume()
        {
            await base.OnResume();
            HttpResponseMessage httpResponse;
            try
            {
                httpResponse = await _apiClient.Execute(HttpMethod.Get, ApiClient.API_BASE + ApiClient.API_PLACES);
            }
            catch
            {
                if (PlaceList.Count == 0)
                {
                    await _dialogService.Value.DisplayAlertAsync("Unable to connect", "", "OK");
                }
                return;
            }
            
            Response<Collection<PlaceItemSummary>> response = await _apiClient.ReadFromResponse<Response<Collection<PlaceItemSummary>>>(httpResponse);
            if (response.IsSuccess)
            {
                Dictionary<int, PlaceItemSummary> placeById = PlaceList.ToDictionary(x => x.Id, x => x);
                foreach (PlaceItemSummary place in response.Data)
                {
                    if (!placeById.ContainsKey(place.Id))
                    {
                        place.ClickPlaceCommand = ClickPlaceCommand;
                        PlaceList.Insert(0, place);
                    }
                }
            }
            else if (PlaceList.Count == 0)
            {
                await _dialogService.Value.DisplayAlertAsync("Unable to retrieve places", "", "OK");
            }
        }

        public async void ClickPlaceActionAsync(PlaceItemSummary place)
        {
            await _navigationService.Value.PushAsync<PlaceItemPage>(new Dictionary<string, object>()
            {
                { "PlaceItemSummary", place }
            });
        }

        public async void ClickNewPlaceActionAsync()
        {
            await _navigationService.Value.PushAsync<NewPlacePage>();
        }
    }
}
