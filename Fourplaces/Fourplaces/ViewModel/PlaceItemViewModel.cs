﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using Fourplaces.DTO;
using Fourplaces.View;
using Storm.Mvvm;
using Storm.Mvvm.Navigation;
using Storm.Mvvm.Services;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Fourplaces.ViewModel
{
    public class PlaceItemViewModel : ViewModelBase
    {
        private readonly Lazy<INavigationService> _navigationService;

        private PlaceItemSummary _place;

        [NavigationParameter("PlaceItemSummary")]
        private PlaceItemSummary PlaceItemSummary
        {
            get;
            set;
        }

        public PlaceItemSummary Place
        {
            get => _place;
            set => SetProperty(ref _place, value);
        }

        public Map PlaceItemMap
        {
            get;
        }

        public ICommand CommentsCommand
        {
            get;
        }

        public PlaceItemViewModel()
        {
            _navigationService = new Lazy<INavigationService>(() => DependencyService.Resolve<INavigationService>());

            PlaceItemMap = new Map();

            CommentsCommand = new Command<PlaceItemSummary>(CommentsActionAsync);
        }

        public override void Initialize(Dictionary<string, object> navigationParameters)
        {
            base.Initialize(navigationParameters);
            Place = PlaceItemSummary;
        }

        public override async Task OnResume()
        {
            await base.OnResume();

            Position position = new Position(Place.Latitude, Place.Longitude);
            MapSpan mapSpan = new MapSpan(position, 0.01, 0.01);
            PlaceItemMap.MoveToRegion(mapSpan);

            if (PlaceItemMap.Pins.Count == 0)
            {
                PlaceItemMap.Pins.Add(new Pin
                {
                    Label = Place.Title,
                    Type = PinType.Place,
                    Position = position
                });
            }
        }

        public async void CommentsActionAsync(PlaceItemSummary place)
        {
            await _navigationService.Value.PushAsync<CommentListPage>(new Dictionary<string, object>()
            {
                { "PlaceItemSummary", place }
            });
        }
    }
}
