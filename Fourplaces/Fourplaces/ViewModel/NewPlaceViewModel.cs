﻿using System;
using System.IO;
using System.Net.Http;
using System.Windows.Input;
using Fourplaces.API;
using Fourplaces.DTO;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Fourplaces.ViewModel
{
    class NewPlaceViewModel : ViewModelBase
    {
        private readonly Lazy<INavigationService> _navigationService;
        private readonly Lazy<IDialogService> _dialogService;
        private readonly ApiClient _apiClient;

        private string _title;
        private string _description;
        private double _latitude;
        private double _longitude;
        private string _coordinatesText;
        private bool _coordinatesEnabled;
        private bool _uploadMessageVisible;
        private int _imageId;
        private bool _imageVisible;
        private bool _photoButtonsVisible;

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        public string Description
        {
            get => _description;
            set => SetProperty(ref _description, value);
        }

        public double Latitude
        {
            get => _latitude;
            set => SetProperty(ref _latitude, value);
        }

        public double Longitude
        {
            get => _longitude;
            set => SetProperty(ref _longitude, value);
        }

        public string CoordinatesText
        {
            get => _coordinatesText;
            set => SetProperty(ref _coordinatesText, value);
        }

        public bool CoordinatesEnabled
        {
            get => _coordinatesEnabled;
            set => SetProperty(ref _coordinatesEnabled, value);
        }

        public bool UploadMessageVisible
        {
            get => _uploadMessageVisible;
            set => SetProperty(ref _uploadMessageVisible, value);
        }

        public int ImageId
        {
            get => _imageId;
            set => SetProperty(ref _imageId, value);
        }

        public bool ImageVisible
        {
            get => _imageVisible;
            set => SetProperty(ref _imageVisible, value);
        }

        public bool PhotoButtonsVisible
        {
            get => _photoButtonsVisible;
            set => SetProperty(ref _photoButtonsVisible, value);
        }

        public ICommand CoordinatesCommand
        {
            get;
        }

        public ICommand TakePhotoCommand
        {
            get;
        }

        public ICommand AddImageFromGalleryCommand
        {
            get;
        }

        public ICommand SavePlaceCommand
        {
            get;
        }

        public NewPlaceViewModel()
        {
            _navigationService = new Lazy<INavigationService>(() => DependencyService.Resolve<INavigationService>());
            _dialogService = new Lazy<IDialogService>(() => DependencyService.Resolve<IDialogService>());
            _apiClient = new ApiClient();

            CoordinatesText = "Retrieve coordinates";
            CoordinatesEnabled = true;

            UploadMessageVisible = false;
            ImageVisible = false;
            PhotoButtonsVisible = true;

            CoordinatesCommand = new Command(CoordinatesActionAsync);
            TakePhotoCommand = new Command(TakePhotoActionAsync);
            AddImageFromGalleryCommand = new Command(AddImageFromGalleryActionAsync);
            SavePlaceCommand = new Command(SavePlaceActionAsync);
        }

        public async void CoordinatesActionAsync()
        {
            CoordinatesEnabled = false;
            CoordinatesText = "Retrieving coordinates...";

            try
            {
                GeolocationRequest request = new GeolocationRequest(GeolocationAccuracy.Medium);
                Xamarin.Essentials.Location location = await Geolocation.GetLocationAsync(request);
                if (location != null)
                {
                    Latitude = location.Latitude;
                    Longitude = location.Longitude;
                }
                else
                {
                    await _dialogService.Value.DisplayAlertAsync("Coordinates", "Unable to retrieve coordinates.", "OK");
                }
            }
            catch
            {
                await _dialogService.Value.DisplayAlertAsync("Coordinates", "Unable to retrieve coordinates.", "OK");
            }

            CoordinatesText = "Retrieve coordinates";
            CoordinatesEnabled = true;
        }

        public async void TakePhotoActionAsync()
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await _dialogService.Value.DisplayAlertAsync("Unable to take photo", "", "OK");
                return;
            }
            MediaFile file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                Directory = "Fourplaces",
                Name = "capture.jpg",
                PhotoSize = PhotoSize.Small
            });
            if (file == null)
            {
                return;
            }
            UploadImageAsync(file);
        }

        public async void AddImageFromGalleryActionAsync()
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await _dialogService.Value.DisplayAlertAsync("Unable to add image from gallery", "", "OK");
                return;
            }
            MediaFile file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
            {
                PhotoSize = PhotoSize.Small
            });
            if (file == null)
            {
                return;
            }
            UploadImageAsync(file);
        }

        public async void UploadImageAsync(MediaFile file)
        {
            PhotoButtonsVisible = false;
            UploadMessageVisible = true;

            MemoryStream memoryStream = new MemoryStream();
            file.GetStream().CopyTo(memoryStream);
            string token = await SecureStorage.GetAsync("token");

            Response<ImageItem> response;
            try
            {
                response = await _apiClient.SendImageAsync(memoryStream.ToArray(), token);
            }
            catch
            {
                await _dialogService.Value.DisplayAlertAsync("Unable to send image", "", "OK");
                UploadMessageVisible = false;
                PhotoButtonsVisible = true;
                return;
            }

            if (response.IsSuccess)
            {
                UploadMessageVisible = false;
                ImageId = response.Data.Id;
                ImageVisible = true;
            }
            else
            {
                await _dialogService.Value.DisplayAlertAsync("Unable to send image", "", "OK");
                UploadMessageVisible = false;
                PhotoButtonsVisible = true;
            }
        }

        public async void SavePlaceActionAsync()
        {
            if (string.IsNullOrEmpty(Title) || string.IsNullOrEmpty(Description) || !ImageVisible || Latitude == 0 || Longitude == 0)
            {
                await _dialogService.Value.DisplayAlertAsync("Missing fields", "", "OK");
                return;
            }
            CreatePlaceRequest createPlaceRequest = new CreatePlaceRequest()
            {
                Title = Title,
                Description = Description,
                ImageId = ImageId,
                Latitude = Latitude,
                Longitude = Longitude
            };
            string token = await SecureStorage.GetAsync("token");
            HttpResponseMessage httpResponse;
            try
            {
                httpResponse = await _apiClient.Execute(HttpMethod.Post, ApiClient.API_BASE + ApiClient.API_PLACES, createPlaceRequest, token);
            }
            catch
            {
                await _dialogService.Value.DisplayAlertAsync("Unable to connect", "", "OK");
                return;
            }
            
            Response response = await _apiClient.ReadFromResponse<Response>(httpResponse);
            if (response.IsSuccess)
            {
                await _dialogService.Value.DisplayAlertAsync("Place saved successfully!", "", "OK");
                await _navigationService.Value.PopAsync();
            }
            else
            {
                await _dialogService.Value.DisplayAlertAsync("Unable to save place", "", "OK");
            }
        }
    }
}
