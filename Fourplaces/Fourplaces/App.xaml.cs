﻿using Fourplaces.View;
using Storm.Mvvm;

namespace Fourplaces
{
    public partial class App : MvvmApplication
    {
        public App() : base(() => new AuthenticationPage())
        {
            InitializeComponent();
        }
    }
}
