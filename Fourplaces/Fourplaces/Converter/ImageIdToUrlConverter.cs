﻿using System;
using System.Globalization;
using Fourplaces.API;
using Xamarin.Forms;

namespace Fourplaces.Converter
{
    public class ImageIdToUrlConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ApiClient.API_BASE + ApiClient.API_IMAGES + "/" + value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
