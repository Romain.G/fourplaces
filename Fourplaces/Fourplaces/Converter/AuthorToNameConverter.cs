﻿using System;
using System.Globalization;
using Fourplaces.DTO;
using Xamarin.Forms;

namespace Fourplaces.Converter
{
    public class AuthorToNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            UserItem author = (UserItem) value;
            return author.FirstName + " " + author.LastName;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
