﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace Fourplaces.Converter
{
    class DoubleToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double n = (double) value;
            return n.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string n = (string) value;
            try
            {
                return double.Parse(n);
            }
            catch
            {
                return 0;
            }
        }
    }
}
